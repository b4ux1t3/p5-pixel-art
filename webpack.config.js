const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCopyPlugin = require('copy-webpack-plugin');


module.exports = {
  entry: './src/index.ts',
  devtool: 'inline-source-map',
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist/'),
    },
    compress: true,
    port: 9000,
  },
  plugins: [
    new WebpackCopyPlugin({
        patterns: [
            {from: "assets/", to: "static"},
            {from: "src/index.html"}
        ]
    })
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'static/script/index.js',
    path: path.resolve(__dirname, 'dist/'),
  },
};
