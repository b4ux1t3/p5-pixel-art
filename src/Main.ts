import * as p5 from 'p5';
import { CanvasManager } from './CanvasManager';
import { SettingsManager } from './SettingsManager';

let cm: CanvasManager;
let sm: SettingsManager;
/*
Represents the p5.js "sketch" of our app. This is basically the "entry point" to our p5.js
application, similar to when you have just a screen with the setup and draw functions.
*/
export function newSketch(sketch: p5): void {

  cm = new CanvasManager(sketch);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  sm = new SettingsManager();
  // We shouldn't do too much in preload or setup, since most of the state is managed by our CanvasManager class.
  sketch.preload = () => {
    cm.preload();
  }

  sketch.setup = () => {
    cm.setup();
  }

  // We'll let our CanvasManager handle drawing.
  sketch.draw = () => {
    cm.draw();
  }

  sketch.mouseClicked = () => {
    cm.mouseClicked(sketch.mouseX, sketch.mouseY);
  }

  sketch.mouseDragged = () => {
    cm.mouseDragged(sketch.mouseX, sketch.mouseY);
  }
}
