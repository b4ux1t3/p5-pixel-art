export function getBase64P5Image(image, p5) {

  image.loadPixels();
  const newImage = p5.createImage(image.width, image.height);
  newImage.copy(image, 0, 0, image.width, image.height, 0, 0, image.width, image.height);
  newImage.resize(300,0);
  return newImage.canvas.toDataURL('image/png');
}
