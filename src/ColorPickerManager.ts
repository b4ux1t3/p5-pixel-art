import { mapRange, parseHexColor, PixelColor, pixelColorToHex, validateHexColorText } from './Utilities';

export type SetColorCallback = (r: number, g: number, b: number) => boolean;
export class ColorPickerManager{

  private rgbDiv: HTMLElement;
  private colorPickerDiv: HTMLElement

  private colorModeSwitch: HTMLInputElement;
  private colorPreview: HTMLElement;
  private get _colorMode(): boolean {
    return this.colorModeSwitch.checked;
  }
  private colorModeLabel: HTMLLabelElement;

  private colorText: HTMLInputElement;

  private sliderRed: HTMLInputElement;
  private sliderGreen: HTMLInputElement;
  private sliderBlue: HTMLInputElement;

  private colorPicker: HTMLInputElement;

  constructor(private setColorCallback: SetColorCallback){
    this.registerElements();
    this.registerListeners();

    this.setColorMode();
    this.colorPickerChange();
  }

  public setColor(color: PixelColor){
    this.setSliders(color);
    const colorString = pixelColorToHex(color);
    this.colorText.value = colorString;
    this.setColorPreview(color);
    this.colorPicker.value = colorString;
  }

  public setColorMode(): void {
    this.colorModeLabel.innerText = this._colorMode ? 'Color Picker' : 'RGB 256-Color';

    this.rgbDiv.hidden = this._colorMode;
    this.colorPickerDiv.hidden = !this._colorMode;

    if (!this._colorMode) this.setColorFromSlider();
  }

  private registerElements(){
    this.rgbDiv = document.querySelector('#rgb-controls');
    this.colorPickerDiv = document.querySelector('#color-picker-controls');
    this.colorModeSwitch = document.querySelector('#color-control-mode-switch');
    this.colorModeLabel = document.querySelector('#color-mode-label')
    this.colorText = document.querySelector('#color-text');
    this.sliderRed = document.querySelector('#red-slider');
    this.sliderGreen = document.querySelector('#green-slider');
    this.sliderBlue = document.querySelector('#blue-slider');
    this.colorPreview = document.querySelector('#color-preview');
    this.colorPicker = document.querySelector('#color-picker');
  }

  private registerListeners(){
    this.colorModeSwitch.addEventListener('change', this.setColorMode.bind(this));
    this.colorText.addEventListener('blur', this.colorTextUpdated.bind(this));
    this.sliderRed.addEventListener('change', this.setColorFromSlider.bind(this));
    this.sliderGreen.addEventListener('change', this.setColorFromSlider.bind(this));
    this.sliderBlue.addEventListener('change', this.setColorFromSlider.bind(this));
    this.colorPicker.addEventListener('change', this.colorPickerChange.bind(this));
  }

  private colorTextUpdated(e: FocusEvent) {
    const result = validateHexColorText(e);
    if (result) {
      const color = parseHexColor(this.colorText.value);
      this.setColor(color);
      this.setColorCallback(color.r, color.g, color.b);
    }
  }

  private setSliders(color: PixelColor){
    this.sliderRed.value = Math.round(mapRange(0,16, 0, 255, color.r)).toString();
    this.sliderGreen.value = Math.round(mapRange(0,16, 0, 255, color.g)).toString();
    this.sliderBlue.value = Math.round(mapRange(0,4, 0, 255, color.b)).toString();
  }

  private setColorFromSlider(){
    const newColor = {
      r: Math.max(parseInt(this.sliderRed.value) * 16 - 1, 0),
      g: Math.max(parseInt(this.sliderGreen.value) * 16 - 1, 0),
      b: Math.max(parseInt(this.sliderBlue.value) * 64 - 1, 0)
    };
    this.setColor(newColor);

    this.setColorCallback(newColor.r, newColor.g, newColor.b);
  }

  private setColorPreview(c: PixelColor){
    this.colorPreview.style.backgroundColor = pixelColorToHex(c);
  }

  private colorPickerChange(){
    const color = parseHexColor(this.colorPicker.value);
    this.setColor(color);
    this.setColorCallback(color.r, color.g, color.b);
  }
}
