import { Color } from 'p5';
export class Cell {

  private eraseColor: Color;

  constructor(public index: number, public CellColor: Color, ){
    this.eraseColor = CellColor;
  }

  public erase(){
    this.CellColor = this.eraseColor;
  }

  public color(newColor: Color){
    this.CellColor = newColor;
  }
}
