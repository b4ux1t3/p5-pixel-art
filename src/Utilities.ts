import * as p5 from "p5";
import { Color } from "p5";

export type PixelColor = {r: number, g: number, b: number}
export function toHex(c: Color, p5: p5) {
  const r = `${p5.red(c) < 16 ? '0' : ''}${p5.red(c).toString(16)}`;
  const g = `${p5.green(c) < 16 ? '0' : ''}${p5.green(c).toString(16)}`;
  const b = `${p5.blue(c) < 16 ? '0' : ''}${p5.blue(c).toString(16)}`;
  return `#${r}${g}${b}`;
}
export function pixelColorToHex(c: PixelColor): string {
  const r = `${c.r < 16 ? '0' : ''}${c.r.toString(16)}`;
  const g = `${c.g < 16 ? '0' : ''}${c.g.toString(16)}`;
  const b = `${c.b < 16 ? '0' : ''}${c.b.toString(16)}`;
  return `#${r}${g}${b}`;
}

export function luminence(c: Color, p5: p5){
  return (p5.red(c) + p5.green(c) + p5.blue(c)) / 3;
}

export function pixelToIndex(pX: number, pY: number, cellSize: number, gridSize: number){
  const x = Math.floor(pX / cellSize);
  const y = Math.floor(pY / cellSize);
  return y * gridSize + x;
}

export function mouseOnCanvas(pX: number, pY: number, cellSize: number, gridSize: number){
  return !(pY > gridSize * cellSize || pY < 0 || pX > gridSize * cellSize || pX < 0);

}

export const validateHexColorText = (e: FocusEvent) : boolean => {
  const target = e.target as HTMLInputElement;
  const value = target.value;
  const regex = new RegExp(/^#([a-fA-F0-9]{3}|[a-fA-F0-9]{6})$/);
  const result = regex.test(value)
  result ? target.classList.remove("invalid") : target.classList.add("invalid");

  return result;
}

export const parseHexColor = (hex: string): PixelColor => {
  if (hex.length === 4){
      const r = parseInt(hex.slice(1,2) + hex.slice(1,2), 16);
      const g = parseInt(hex.slice(2,3) + hex.slice(2,3), 16);
      const b = parseInt(hex.slice(3) + hex.slice(3), 16);
    return {
      r,
      g,
      b,
    };
  }
  if (hex.length === 7) {
    return {
      r: parseInt(hex.slice(1,3), 16),
      g: parseInt(hex.slice(3,5), 16),
      b: parseInt(hex.slice(5), 16),
    };
  }
}

export function mapRange(minTo: number, maxTo: number, minFrom: number, maxFrom: number, n: number){

  const fromRange = maxFrom - minFrom;
  const distance = n / fromRange;
  const toRange = maxTo - minTo;
  return minTo + (toRange * distance);

}
