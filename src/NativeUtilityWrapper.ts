import * as p5 from "p5";
import { getBase64P5Image } from './nativeUtilities';


export function getBase64OfP5Image(image: p5.Image, p5: p5) : string {
  return (getBase64P5Image(image, p5) as string);
}
