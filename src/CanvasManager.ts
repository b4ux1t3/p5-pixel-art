import * as p5 from 'p5';
import { pixelToIndex, mouseOnCanvas, PixelColor } from './Utilities';
import { Cell } from './Cell';
import { ColorPickerManager } from './ColorPickerManager';
import { getBase64P5Image } from './nativeUtilities';

export class CanvasManager{
  private colorPickerManager: ColorPickerManager;

  private canvasWidth = 800;
  private canvasHeight = 800;

  private gridSize = 16;
  private get _cellSize(): number {
    return Math.min(this.canvasWidth, this.canvasHeight) / this.gridSize;
  }

  private sketch: p5;
  private canvas: p5.Renderer;
  private cells: Cell[];
  private bgImg: p5.Image;

  private eyedropperCheckbox: p5.Element;
  private get _eyedropper(): boolean{
    return (this.eyedropperCheckbox.elt as HTMLInputElement).checked;
  }
  private gridCheckbox: p5.Element;
  private get _showGrid(): boolean {
    return (this.gridCheckbox.elt as HTMLInputElement).checked;
  }
  private eraserCheckbox: p5.Element;
  private get _eraser(): boolean {
    return (this.eraserCheckbox.elt as HTMLInputElement).checked;
  }


  private saveButton: p5.Element;
  private clearButton: p5.Element;

  private canvasSlider: p5.Element;
  private canvasSizeSpan: p5.Element;

  private render: p5.Image;
  private previewImage: p5.Element;

  private currentColor: p5.Color;
  private get _pixelColor(): PixelColor {
    return this.toPixelColor(this.currentColor);
  }

  constructor(sketch: p5){
    this.sketch = sketch;
  }

  public preload() {
    this.bgImg = this.sketch.loadImage('static/img/transparency_background.svg');
  }

  public setup(){
    this.colorPickerManager = new ColorPickerManager(this.setColor.bind(this));

    this.cells = [];
    for (let i = 0; i < this.gridSize * this.gridSize; i++){
      this.cells.push(new Cell(i, this.sketch.color(0,0,0,0)));
    }

    this.canvas = this.sketch.createCanvas(this.canvasWidth, this.canvasHeight);
    this.canvas.parent('canvas');

    this.render = this.sketch.createImage(this.gridSize, this.gridSize);

    this.currentColor = this.sketch.color(127);

    this.setupUi();

    // The following is some hackery to make this work on mobile devcies.
    const canvasDom = document.querySelector('.p5Canvas');
    canvasDom.addEventListener("touchstart",  (event) => {event.preventDefault()});
    canvasDom.addEventListener("touchmove",   (event) => {event.preventDefault()});
    canvasDom.addEventListener("touchend",    (event) => {event.preventDefault()});
    canvasDom.addEventListener("touchcancel", (event) => {event.preventDefault()});
    canvasDom.addEventListener("dblclick",    () => {console.log('doubleclick');return false;});
  }

  public draw() {
    this.sketch.background(255);
    this.sketch.background(this.bgImg);
    this.cells.forEach(c => this.drawCell(c));
  }

  public mouseClicked(mouseX: number, mouseY: number){
    if (!mouseOnCanvas(mouseX, mouseY, this._cellSize, this.gridSize)) return;

    const index = pixelToIndex(mouseX, mouseY, this._cellSize, this.gridSize);
    if (this._eyedropper) {
      this.eyedropper(index);
      return;
    }
    if (this._eraser) {
      this.cells[index].erase();
      return;
    }

    this.cells[index].color(this.currentColor);
    this.getRenderCanvas();
  }

  public mouseDragged(mouseX: number, mouseY: number){
    if (!mouseOnCanvas(mouseX, mouseY, this._cellSize, this.gridSize)) return;

    const index = pixelToIndex(mouseX, mouseY, this._cellSize, this.gridSize);
    if (this._eraser) {
      this.cells[index].erase();
      return;
    }

    this.cells[index].color(this.currentColor);
    this.getRenderCanvas();
  }

  public setColor(r: number, g: number, b: number) : boolean {
    if (r > 255 || r < 0 || g > 255 || g < 0 || b > 255 || b < 0) return false;

    this.currentColor = this.sketch.color(r, g, b);
    return true;
  }

  private setupUi(){
    this.eyedropperCheckbox = this.sketch.select('#eyedropper-check');
    this.gridCheckbox = this.sketch.select('#grid-check');
    this.eraserCheckbox = this.sketch.select('#eraser-check');

    this.saveButton = this.sketch.select('#save-button');
    this.saveButton.mousePressed(this.dumpRender.bind(this));
    this.clearButton = this.sketch.select('#clear-button');
    this.clearButton.mousePressed(() => this.cells.forEach(c => c.erase()));

    this.canvasSlider = this.sketch.select('#canvas-slider');
    this.canvasSlider.mouseMoved(this.updateCanvas.bind(this))
    this.canvasSlider.value(this.canvasWidth);
    this.canvasSizeSpan = this.sketch.select('#canvas-size-span');
    this.canvasSizeSpan.html(`${this.canvasWidth}px`);

    this.previewImage = this.sketch.createImg('', 'Preview');
    this.previewImage.parent('preview');
    document.getElementsByName('grid-size').forEach(r => r.addEventListener('change', this.changeResolution.bind(this)));
    this.getRenderCanvas();

    this.updateColorPicker();
  }

  private drawCell(cell: Cell): void {
    this.sketch.fill(cell.CellColor);
    const x = (cell.index % this.gridSize) * this._cellSize;
    const y = (Math.floor(cell.index / this.gridSize)) * this._cellSize;
    if (this._showGrid) this.sketch.stroke(0);
    else this.sketch.noStroke();

    this.sketch.rect(x, y, this._cellSize, this._cellSize);
  }

  private eyedropper(index: number) {
    this.currentColor = this.cells[index].CellColor;
    this.eyedropperCheckbox.elt.checked = false;

    this.updateColorPicker();
  }

  private dumpRender(){
    this.getRenderCanvas();
    this.sketch.save(this.render, 'render.png')
  }

  private toPixelColor(color: p5.Color) : PixelColor{
    return {
      r: this.sketch.red(color),
      g: this.sketch.green(color),
      b: this.sketch.blue(color),
    };
  }

  private updateColorPicker() {
    this.colorPickerManager.setColor(this._pixelColor);
  }

  private updateCanvas(e: InputEvent) {
    const val = parseInt((e.target as HTMLInputElement).value);
    this.canvasWidth = val;
    this.sketch.resizeCanvas(val, val);
    this.canvasSizeSpan.html(`${this.canvasWidth}px`);
  }

  private getRenderCanvas(){
    this.render.loadPixels();
    for (let i = 0; i < this.render.pixels.length; i += 4){
      const index = i / 4;
      this.render.pixels[i] = this.sketch.red(this.cells[index].CellColor);
      this.render.pixels[i+1] = this.sketch.green(this.cells[index].CellColor);
      this.render.pixels[i+2] = this.sketch.blue(this.cells[index].CellColor);
      this.render.pixels[i+3] = this.sketch.alpha(this.cells[index].CellColor);
    }
    this.render.updatePixels();
    const base64: string = getBase64P5Image(this.render, this.sketch);
    (this.previewImage.elt as HTMLImageElement).src = base64;
  }

  private changeResolution(){
    const checkedValue = parseInt((document.querySelector('input[name="grid-size"]:checked') as HTMLInputElement).value);
    this.gridSize = checkedValue;
    this.cells = [];
    this.render = this.sketch.createImage(this.gridSize, this.gridSize);
    for (let i = 0; i < this.gridSize * this.gridSize; i++){
      this.cells.push(new Cell(i, this.sketch.color(0,0,0,0)));
    }
  }
}
