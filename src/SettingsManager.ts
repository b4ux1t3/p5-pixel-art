export class SettingsManager{
  private settingsButton: HTMLButtonElement;
  private settingsDiv: HTMLElement;
  private showMenu = false;

  constructor(){
    this.settingsButton = document.querySelector('#menu-button');
    this.settingsDiv = document.querySelector('#menu-dropdown');
    this.settingsButton.addEventListener('click', this.toggleMenu.bind(this));
  }

  private toggleMenu(){
    this.showMenu = !this.showMenu;
    if (this.showMenu){
      this.settingsButton.classList.add('show');
      this.settingsDiv.classList.add('show');
    } else {
      this.settingsButton.classList.remove('show');
      this.settingsDiv.classList.remove('show');
    }
  }
}
