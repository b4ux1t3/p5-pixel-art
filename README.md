[![first-timers-only](https://img.shields.io/badge/first--timers--only-friendly-blue.svg?style=flat-square)](https://www.firsttimersonly.com/)
[![hosted-on-gitlabio](https://img.shields.io/badge/hosted%20on-GitLab.io-orange)](https://b4ux1t3.gitlab.io/p5-pixel-art)

# Untitled Pixel Art Editor

This is a very simple pixel art editor. You can access it right here on GitLab.
