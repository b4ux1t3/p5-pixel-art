module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
      '@typescript-eslint',
    ],
    extends: [
      'eslint:recommended',
      'plugin:@typescript-eslint/recommended',
    ],
    overrides: [
      {
        files: ['*.ts'],
        rules: {
          "@typescript-eslint/explicit-member-accessibility": ["error", {
            accessibility: 'explicit',
            overrides:
              {
                accessors: 'explicit',
                constructors: 'no-public',
                methods: 'explicit',
                properties: 'explicit',
                parameterProperties: 'explicit'
              }
          }]
        }
      }
    ]
  };
